#include "bpred.h"

#define TAKEN   true
#define NOTTAKEN false
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
uint32_t policy_chosen = 0; 

BPRED::BPRED(uint32_t policy) //like the constructor, everything gets initialized here
{
	policy_chosen = policy; 
	ghr = 0; 
	for(int i = 0; i < 4096; i ++)
	{
		pht[i]=2; 
	} 
  
}

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

bool BPRED::GetPrediction(uint32_t PC){
    
    if(policy_chosen == 1)//if the predictor is the always taken predictor then
    {
    	return TAKEN; 
    }
    else if(policy_chosen == 2) //this is the GHSARE predictor
    {
    	// lets first XOR the bottom 12 bits of the PC to access the PHT 
	//first take the bottom 12 bits
	uint16_t bottom_address = PC & 0x0FFF; //will give us the bottom 12 bits of the address
	uint16_t index = bottom_address ^ ghr ; 
	//now lets index into the pht and find the result 
	if(pht[index] == 3 || pht[index] == 2)
	{
		return TAKEN; 
	}
	else if(pht[index] == 1 || pht[index] == 0)
	{
		return NOTTAKEN; 
	} 
	else
	{
		//printf("some problem in pht registers brother\n");
		return TAKEN;
	} 
    }

    else
    {
    	//printf("some problem in get prediction function or its getting the policy value when it shouldnt");

    return TAKEN;  
	}
}


/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

void  BPRED::UpdatePredictor(uint32_t PC, bool resolveDir, bool predDir) {

	if(policy_chosen == 2) //only then there is a need to update the predictor 
	{
		//get the index first 
		uint16_t bottom_address = PC & 0x0FFF; //will give us the bottom 12 bits of the address
		uint16_t index = bottom_address ^ ghr ; 
		//now lets update the pht 
		if(resolveDir == TAKEN)
		{
			if(pht[index] < 3)
			{
				pht[index]++; 
			}
		}
		else
		{
			if(pht[index] > 0)
			{
				pht[index] -- ;
			}
		}
		//now lets update the ghr itself 
		if(resolveDir == TAKEN)
		{
			ghr = (ghr << 1) + 1; 
			
		}
		else
		{
			ghr = ghr << 1; 
		}
		ghr = ghr & 0x0FFF; 
	}


}

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

