/***********************************************************************
 * File         : pipeline.cpp
 * Author       : Soham J. Desai 
 * Date         : 14th January 2014
 * Description  : Superscalar Pipeline for Lab2 ECE 6100
 **********************************************************************/

#include "pipeline.h"
#include <cstdlib>

extern int32_t PIPE_WIDTH;
extern int32_t ENABLE_MEM_FWD;
extern int32_t ENABLE_EXE_FWD;
extern int32_t BPRED_POLICY;

/**********************************************************************
 * Support Function: Read 1 Trace Record From File and populate Fetch Op
 **********************************************************************/

void pipe_get_fetch_op(Pipeline *p, Pipeline_Latch* fetch_op){
    uint8_t bytes_read = 0;
    bytes_read = fread(&fetch_op->tr_entry, 1, sizeof(Trace_Rec), p->tr_file);

    // check for end of trace
    if( bytes_read < sizeof(Trace_Rec)) {
      fetch_op->valid=false;
      p->halt_op_id=p->op_id_tracker;
      return;
    }

    // got an instruction ... hooray!
    fetch_op->valid=true;
    fetch_op->stall=false;
    fetch_op->is_mispred_cbr=false;
    p->op_id_tracker++;
    fetch_op->op_id=p->op_id_tracker;
    
    return; 
}


/**********************************************************************
 * Pipeline Class Member Functions 
 **********************************************************************/

Pipeline * pipe_init(FILE *tr_file_in){
    printf("\n** PIPELINE IS %d WIDE **\n\n", PIPE_WIDTH);

    // Initialize Pipeline Internals
    Pipeline *p = (Pipeline *) calloc (1, sizeof (Pipeline));

    p->tr_file = tr_file_in;
    p->halt_op_id = ((uint64_t)-1) - 3;           

    // Allocated Branch Predictor
    if(BPRED_POLICY){
      p->b_pred = new BPRED(BPRED_POLICY);
    }

    return p;
}


/**********************************************************************
 * Print the pipeline state (useful for debugging)
 **********************************************************************/

void pipe_print_state(Pipeline *p){
    std::cout << "--------------------------------------------" << std::endl;
    std::cout <<"cycle count : " << p->stat_num_cycle << " retired_instruction : " << p->stat_retired_inst << std::endl;

    uint8_t latch_type_i = 0;   // Iterates over Latch Types
    uint8_t width_i      = 0;   // Iterates over Pipeline Width
    for(latch_type_i = 0; latch_type_i < NUM_LATCH_TYPES; latch_type_i++) {
        switch(latch_type_i) {
            case 0:
                printf(" FE: ");
                break;
            case 1:
                printf(" ID: ");
                break;
            case 2:
                printf(" EX: ");
                break;
            case 3:
                printf(" MEM: ");
                break;
            default:
                printf(" ---- ");
        }
    }
    printf("\n");
    for(width_i = 0; width_i < PIPE_WIDTH; width_i++) {
        for(latch_type_i = 0; latch_type_i < NUM_LATCH_TYPES; latch_type_i++) {
            if(p->pipe_latch[latch_type_i][width_i].valid == true) {
	      printf(" %6u ",(uint32_t)( p->pipe_latch[latch_type_i][width_i].op_id));
            } else {
                printf(" ------ ");
            }
        }
        printf("\n");
    }
    printf("\n");

}


/**********************************************************************
 * Pipeline Main Function: Every cycle, cycle the stage 
 **********************************************************************/

void pipe_cycle(Pipeline *p)
{
    p->stat_num_cycle++;
    //pipe_print_state(p);
    pipe_cycle_WB(p);
    pipe_cycle_MEM(p);
    pipe_cycle_EX(p);
    pipe_cycle_ID(p);
    pipe_cycle_FE(p);
  // pipe_print_state(p);
	    
}

/**********************************************************************
 * -----------  DO NOT MODIFY THE CODE ABOVE THIS LINE ----------------
 **********************************************************************/
bool first_time = true; bool stop_fetch = false; 
uint64_t branch_stall_id = 0;

void pipe_cycle_WB(Pipeline *p){
  int ii;
 if(first_time)
{
	p->fetch_cbr_stall = false; 
}
 for(ii=0; ii<PIPE_WIDTH; ii++){
 if(p->fetch_cbr_stall && p->pipe_latch[MEM_LATCH][ii].op_id == branch_stall_id )//&& p->pipe_latch[MEM_LATCH][ii].valid )
 {  // set fetch_cbr_stall as false and also for heavens sake unstall the fetch latch as well please 
    //p->stat_retired_inst++;
    p->fetch_cbr_stall = false; //first_time = true; 
   /* for(int jj = 0; jj< PIPE_WIDTH; jj++)
    {
    	if(!p->pipe_latch[FE_LATCH][jj].valid && p->pipe_latch[FE_LATCH][jj].op_id > branch_stall_id && !p->pipe_latch[FE_LATCH][jj].stall)
    	{
    		
    		p->pipe_latch[FE_LATCH][jj].stall = false; 
    		p->pipe_latch[FE_LATCH][jj].valid = true; 
    		//jj= PIPE_WIDTH;
    	}

    }*/
     
 }
    if(p->pipe_latch[MEM_LATCH][ii].valid){
      p->stat_retired_inst++;
      p->pipe_latch[MEM_LATCH][ii].valid = false;
      if(p->pipe_latch[MEM_LATCH][ii].op_id >= p->halt_op_id){
	p->halt=true;
	ii = PIPE_WIDTH; 
	break; 
      }
    }
  }
}

//--------------------------------------------------------------------//


//--------------------------------------------------------------------//

//--------------------------------------------------------------------//

void pipe_cycle_MEM(Pipeline *p){
  int ii;
  for(ii=0; ii<PIPE_WIDTH; ii++){

    p->pipe_latch[MEM_LATCH][ii]=p->pipe_latch[EX_LATCH][ii];
    
  }
}

//--------------------------------------------------------------------//

void pipe_cycle_EX(Pipeline *p){
  int ii;
  //only if valid , process the instruction 

  for(ii=0; ii<PIPE_WIDTH; ii++){
    p->pipe_latch[EX_LATCH][ii]=p->pipe_latch[ID_LATCH][ii];      
}
}

bool detect_cbr_dependency(int current_instr,Pipeline *p)
{

// we have detected a branch in this particular instruction, go dance now
	// first check the id latch
	uint64_t current = 0; uint64_t old = p->pipe_latch[ID_LATCH][current_instr].op_id; bool dependency_found = false; 
	for(int find = 0; find < 3*PIPE_WIDTH; find++)
	{
		current = old - 1; 
		for(int k = ID_LATCH; k <= MEM_LATCH; k ++)
		{
			for(int pipe = 0; pipe < PIPE_WIDTH; pipe ++)
			{
				if(p->pipe_latch[k][pipe].op_id == current && p->pipe_latch[k][pipe].valid && p->pipe_latch[k][pipe].tr_entry.cc_write == 1)
				{
					//int l = current; 
					//printf("we got in with the current value of %d whos optype is %d and value of latch is %d", l, p->pipe_latch[k][pipe].tr_entry.op_type, k);
					if((p->pipe_latch[k][pipe].tr_entry.op_type == OP_LD && ENABLE_MEM_FWD && k == MEM_LATCH))
   					{
   							//memory forwarding enabled, nothing to search here, move on to the next location 
   							k = MEM_LATCH+1; 
    						pipe = PIPE_WIDTH;
    						find = 3*PIPE_WIDTH + 10; 
    						dependency_found = false;  
    						break;
   						//printf("forwarding applied");
   					}
   					else if((p->pipe_latch[k][pipe].tr_entry.op_type == OP_ALU) && ((ENABLE_EXE_FWD && k == EX_LATCH) || (ENABLE_MEM_FWD && k == MEM_LATCH)))
   					{
   							//printf("ALU thing checks out\n");
   							k = MEM_LATCH+1; 
    						pipe = PIPE_WIDTH;
    						find = 3*PIPE_WIDTH + 10; 
    						dependency_found = false;  
    						break;
   					}
   					else if((p->pipe_latch[k][pipe].tr_entry.op_type == OP_OTHER) && ((ENABLE_EXE_FWD && k == EX_LATCH) || (ENABLE_MEM_FWD && k == MEM_LATCH)))
   					{
   							k = MEM_LATCH+1; 
    						pipe = PIPE_WIDTH;
    						find = 3*PIPE_WIDTH + 10; 
    						dependency_found = false;  
    						break;
   					}
   					else if((p->pipe_latch[k][pipe].tr_entry.op_type == OP_CBR) && ((ENABLE_EXE_FWD && k == EX_LATCH) || (ENABLE_MEM_FWD && k == MEM_LATCH)))
   					{
   							k = MEM_LATCH+1; 
    						pipe = PIPE_WIDTH;
    						find = 3*PIPE_WIDTH + 10; 
    						dependency_found = false;  
    						break;
   					}
   					else
   					{
   						//there is a dependency, its time to stall 
   							k = MEM_LATCH+1; 
    						pipe = PIPE_WIDTH;
    						find = 3*PIPE_WIDTH + 10; 
    						dependency_found = true;  
    						break;

   					}

				}
			}
		}
		old = current; 
}
return dependency_found;



}
//--------------------------------------------------------------------//

void pipe_cycle_ID(Pipeline *p){
int ii; int jj; int latch; 


for(int j = 0 ; j < PIPE_WIDTH; j ++)
{
	//if(!p->pipe_latch[FE_LATCH][j].stall)
	{
	p->pipe_latch[ID_LATCH][j] = p->pipe_latch[FE_LATCH][j]; 
	p->pipe_latch[ID_LATCH][j].valid  = false; 
	p->pipe_latch[FE_LATCH][j].stall = true; 
}
}

int current_instr = -1; 



for(ii=0; ii<PIPE_WIDTH; ii++)
{
	int oldinstr = current_instr; 

	//find the invalid instr with lowest opid 
	for(int findinv = 0; findinv< PIPE_WIDTH; findinv++)
	{
		if(p->pipe_latch[ID_LATCH][findinv].valid == false)
		{
			/*if(p->pipe_latch[ID_LATCH][findinv].op_id == 1312)
  	{
  		printf("1312 still invalid");
  	}*/
			current_instr = findinv; 
			break; 
		}
	}
	for(int findlow = 0; findlow< PIPE_WIDTH; findlow++)
	{
		if(p->pipe_latch[FE_LATCH][findlow].valid && !p->pipe_latch[ID_LATCH][findlow].valid && p->pipe_latch[ID_LATCH][findlow].op_id < p->pipe_latch[ID_LATCH][current_instr].op_id && findlow!= oldinstr)
		{
			current_instr = findlow; 
		}
	}
	if(p->pipe_latch[FE_LATCH][current_instr].valid)
	{
	bool some_dependency_found = false; bool just_stalled = false; 
	//check for cc dependencies first 
	for(int latch = ID_LATCH; latch <= MEM_LATCH; latch ++)
    {
    	for(int jj = 0; jj < PIPE_WIDTH; jj ++)
    	{
    		//check only if one of the preceeding instructions 
    		if(p->pipe_latch[latch][jj].op_id < p->pipe_latch[ID_LATCH][current_instr].op_id && p->pipe_latch[latch][jj].valid)
   			{
				//lets first check for cc flags 
   				  
   				

   				//now lets check for the src dest stuff 
   				if((!some_dependency_found) && (p->pipe_latch[ID_LATCH][current_instr].tr_entry.src1_needed) && (p->pipe_latch[latch][jj].tr_entry.dest_needed) && ((p->pipe_latch[latch][jj].valid)  ) && (p->pipe_latch[ID_LATCH][current_instr].tr_entry.src1_reg == p->pipe_latch[latch][jj].tr_entry.dest))
   					// && p->pipe_latch[FE_LATCH][ii].tr_entry.op_type != OP_LD        || (!p->pipe_latch[latch][jj].valid && latch == ID_LATCH
   					{
   					//printf("src1 ");
   					if((p->pipe_latch[latch][jj].tr_entry.op_type == OP_LD) && (ENABLE_MEM_FWD) && (latch == MEM_LATCH))
   					{
   						//some_dependency_found = false; //nothing found here all clean
   					}
   					else if((p->pipe_latch[latch][jj].tr_entry.op_type == OP_ALU) && ((ENABLE_EXE_FWD && latch == EX_LATCH) || (ENABLE_MEM_FWD && latch == MEM_LATCH)))
   					{
   						//some_dependency_found = false; //nothing found here all clean
   					}
   					else if((p->pipe_latch[latch][jj].tr_entry.op_type == OP_OTHER) && ((ENABLE_EXE_FWD && latch == EX_LATCH) || (ENABLE_MEM_FWD && latch == MEM_LATCH)))
   					{
   						//some_dependency_found = false; //nothing found here all clean
   					}
   					else if((p->pipe_latch[latch][jj].tr_entry.op_type == OP_CBR) && ((ENABLE_EXE_FWD && latch == EX_LATCH) || (ENABLE_MEM_FWD && latch == MEM_LATCH)))
   					{
   						//some_dependency_found = false; //nothing found here all clean
   					}
   					else if ((p->pipe_latch[latch][jj].tr_entry.op_type == OP_LD || p->pipe_latch[latch][jj].tr_entry.op_type == OP_ST) && (p->pipe_latch[ID_LATCH][current_instr].tr_entry.op_type == OP_LD || p->pipe_latch[ID_LATCH][current_instr].tr_entry.op_type == OP_ST) && ENABLE_MEM_FWD)
   					{

   					}
   					else
   					{
   						some_dependency_found = true; 
   					just_stalled =! p->pipe_latch[FE_LATCH][current_instr].stall ; 

   					//no need to further continue these loops break out 
    				latch = FE_LATCH; 
    				jj = PIPE_WIDTH;
    				break; //break out of inner loop
    			     }
   				}

   				if(!some_dependency_found && p->pipe_latch[ID_LATCH][current_instr].tr_entry.src2_needed && p->pipe_latch[latch][jj].tr_entry.dest_needed && ((p->pipe_latch[latch][jj].valid) ) && p->pipe_latch[ID_LATCH][current_instr].tr_entry.src2_reg == p->pipe_latch[latch][jj].tr_entry.dest)//&&  p->pipe_latch[FE_LATCH][ii].tr_entry.op_type != OP_LD
   				{
   					if((p->pipe_latch[latch][jj].tr_entry.op_type == OP_LD) && (ENABLE_MEM_FWD) && (latch == MEM_LATCH))
   					{
   						//some_dependency_found = false; //nothing found here all clean
   					}
   					if((p->pipe_latch[latch][jj].tr_entry.op_type == OP_LD) && (ENABLE_MEM_FWD) && (latch == MEM_LATCH))
   					{
   						//some_dependency_found = false; //nothing found here all clean
   					}
   					else if((p->pipe_latch[latch][jj].tr_entry.op_type == OP_ALU) && ((ENABLE_EXE_FWD && latch == EX_LATCH) || (ENABLE_MEM_FWD && latch == MEM_LATCH)))
   					{
   						//some_dependency_found = false; //nothing found here all clean
   					}
   					else if((p->pipe_latch[latch][jj].tr_entry.op_type == OP_OTHER) && ((ENABLE_EXE_FWD && latch == EX_LATCH) || (ENABLE_MEM_FWD && latch == MEM_LATCH)))
   					{
   						//some_dependency_found = false; //nothing found here all clean
   					}
   					else if((p->pipe_latch[latch][jj].tr_entry.op_type == OP_CBR) && ((ENABLE_EXE_FWD && latch == EX_LATCH) || (ENABLE_MEM_FWD && latch == MEM_LATCH)))
   					{
   						//some_dependency_found = false; //nothing found here all clean
   					}
   					/*if(p->pipe_latch[ID_LATCH][current_instr].tr_entry.op_type == OP_ST && p->pipe_latch[latch][jj].tr_entry.op_type == OP_ST)
   					{

   					} */
   					//printf("src2 ");
   					else if ((p->pipe_latch[latch][jj].tr_entry.op_type == OP_LD || p->pipe_latch[latch][jj].tr_entry.op_type == OP_ST) && (p->pipe_latch[ID_LATCH][current_instr].tr_entry.op_type == OP_LD || p->pipe_latch[ID_LATCH][current_instr].tr_entry.op_type == OP_ST) && ENABLE_MEM_FWD)
   					{

   					}
   					else
   					{
   					some_dependency_found = true; 
   					just_stalled = !p->pipe_latch[FE_LATCH][current_instr].stall ; 

   					//no need to further continue these loops break out 
    				latch = FE_LATCH; 
    				jj = PIPE_WIDTH;
   					break; //break out of inner loop
   				}
   				}

   			}
    	}
	}

	if((!some_dependency_found) && (p->pipe_latch[ID_LATCH][current_instr].tr_entry.cc_read == 1) ) //cc flag checks
   	{
   		bool dep = detect_cbr_dependency(current_instr, p); 
   		if(dep)
   		{
			some_dependency_found = true; 
		}
  	}
	if(some_dependency_found)
   	{
   		p->pipe_latch[FE_LATCH][current_instr].stall = true; 
   		p->pipe_latch[ID_LATCH][current_instr].valid = false; 
   		ii = PIPE_WIDTH;
   		break; 
   	}
   	else
   	{
   					if(p->pipe_latch[FE_LATCH][current_instr].stall )//&& p->pipe_latch[FE_LATCH][current_instr].valid) //if it was stalled before, unstall it and move it ahead
   					{
   					p->pipe_latch[FE_LATCH][current_instr].stall = false; 
   					p->pipe_latch[ID_LATCH][current_instr] = p -> pipe_latch[FE_LATCH][current_instr]; 
   					//p->pipe_latch[FE_LATCH][current_instr].valid = false; 
   					//p->pipe_latch[ID_LATCH][current_instr].valid = true; 
   					}
   					else
   					{
   						//ignore that stuff 

   					}
   					
   			}	
}
else
{
	p->pipe_latch[FE_LATCH][current_instr].stall = false; 

}
}

}


//--------------------------------------------------------------------//

void pipe_cycle_FE(Pipeline *p){
Pipeline_Latch fetch_op;
bool tr_read_success;
if(!stop_fetch)
{
for(int ii = 0; ii< PIPE_WIDTH; ii ++)
{
  	if(first_time)
  	{
  		if(!p->fetch_cbr_stall) //if the stall has been removed then 
  		{
  		pipe_get_fetch_op(p, &fetch_op);
  		 if(BPRED_POLICY)
    	{
      		pipe_check_bpred(p, &fetch_op);
    	}
    	// copy the op in FE LATCH

    	p->pipe_latch[FE_LATCH][ii]=fetch_op;
    	p->pipe_latch[FE_LATCH][ii].stall=false;
    	p->pipe_latch[FE_LATCH][ii].valid=true;
    }
    	
    else
    {
    	
    	p->pipe_latch[FE_LATCH][ii].valid = false;
    }
    }
  	else
  	{
  		if(!p->fetch_cbr_stall) //if the stall has been removed then 
  		{	
  			if(!p->pipe_latch[FE_LATCH][ii].stall) // && //if the latch hasnt' already been stalled 
  			{
  				//fetch_op.valid = true; 
  				pipe_get_fetch_op(p, &fetch_op);
  				//if(fetch_op.valid)
  				//{
    			if(BPRED_POLICY)
    			{
      				pipe_check_bpred(p, &fetch_op);
    			}
    			// copy the op in FE LATCH
    			p->pipe_latch[FE_LATCH][ii]=fetch_op;
    			p->pipe_latch[FE_LATCH][ii].valid=true;
    			/*if(p->pipe_latch[FE_LATCH][ii].op_id >= p->halt_op_id)
    			{
    				stop_fetch = true; 
    				for(int m = ii+1; m < PIPE_WIDTH; m++)
    				{
    					p->pipe_latch[FE_LATCH][ii].valid = false; 
    				}
    				stop_fetch = true; 
    				break; 
    			}*/
    		//}
    		}
    		else
    		{

    		}

		}
		else //if there indeed is a stall 
		{
			if(p->pipe_latch[FE_LATCH][ii].stall)
    		{ 
    			//p->pipe_latch[FE_LATCH][ii].valid = true;
    			//keep the instruction in the FE latch as is 
    		}
    		else
    		{
    			p->pipe_latch[FE_LATCH][ii].valid = false; 
    		}
		}	
	}
}
	first_time = false; 
}
else
{
	for(int ii = 0 ; ii< PIPE_WIDTH; ii++)
	{
		p->pipe_latch[FE_LATCH][ii].valid = false; 
	}
}
}



//--------------------------------------------------------------------//

void pipe_check_bpred(Pipeline *p, Pipeline_Latch *fetch_op){
  // call branch predictor here, if mispred then mark in fetch_op
  // update the predictor instantly
  // stall fetch using the flag p->fetch_cbr_stall
	if(fetch_op->tr_entry.op_type == OP_CBR) //its a branch, time to get the predictor on
	{

		p->b_pred->stat_num_branches++;
		//get 32 bit address first 
		uint32_t address = 0xFFFFFFFF & fetch_op->tr_entry.inst_addr;
		//call the branch predictors with the fetch op. 
		bool prediction = p->b_pred->GetPrediction(address);
		bool actual_branch = fetch_op->tr_entry.br_dir; 
		p->b_pred->UpdatePredictor(address,actual_branch, prediction);
		if(prediction != actual_branch)
		{
			p->b_pred->stat_num_mispred++;
			p->fetch_cbr_stall = true; 
			branch_stall_id = fetch_op->op_id;
			int m = fetch_op->op_id;
			//printf("stall due to branch began from this instruction %d\n",m);
		}
		else
		{
			
			p->fetch_cbr_stall = false; 
			
		}
	}







}


//--------------------------------------------------------------------//

