#include <stdio.h>
#include <assert.h>

#include "rob.h"


extern int32_t NUM_ROB_ENTRIES;

/////////////////////////////////////////////////////////////
// Init function initializes the ROB
/////////////////////////////////////////////////////////////

ROB* ROB_init(void){
  int ii;
  ROB *t = (ROB *) calloc (1, sizeof (ROB));
  for(ii=0; ii<MAX_ROB_ENTRIES; ii++){
    t->ROB_Entries[ii].valid=false;
    t->ROB_Entries[ii].ready=false;
  }
  t->head_ptr=0;
  t->tail_ptr=0;
  return t;
}

/////////////////////////////////////////////////////////////
// Print State
/////////////////////////////////////////////////////////////
void ROB_print_state(ROB *t){
 int ii = 0;
  printf("Printing ROB \n");
  printf("Entry  Inst   Valid   ready\n");
  for(ii = 0; ii < 7; ii++) {
    printf("%5d ::  %d\t", ii, (int)t->ROB_Entries[ii].inst.inst_num);
    printf(" %5d\t", t->ROB_Entries[ii].valid);
    printf(" %5d\n", t->ROB_Entries[ii].ready);
  }
  printf("\n");
}

/////////////////////////////////////////////////////////////
//------- DO NOT CHANGE THE CODE ABOVE THIS LINE -----------
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// If there is space in ROB return true, else false
/////////////////////////////////////////////////////////////

bool ROB_check_space(ROB *t){
	for(int index = 0; index < NUM_ROB_ENTRIES; index ++)
	{
		if(!t->ROB_Entries[index].valid) //if not valid, it means there is space 
		{
			return true; 
		}
	}
	return false; //means no empty space 

}

/////////////////////////////////////////////////////////////
// insert entry at tail, increment tail (do check_space first)
/////////////////////////////////////////////////////////////

int ROB_insert(ROB *t, Inst_Info inst){
	if(ROB_check_space(t)) //if there is space 
	{
		if(t->tail_ptr == NUM_ROB_ENTRIES) //if its reached at the bottom
		{
			t->ROB_Entries[0].inst = inst; 
			t->ROB_Entries[0].valid = true; 
			t->ROB_Entries[0].ready = false; 
			t->tail_ptr = 1; 
		}
		else
		{
			t->ROB_Entries[t->tail_ptr].inst = inst;
			t->ROB_Entries[t->tail_ptr].valid = true;  
			t->ROB_Entries[t->tail_ptr].ready = false; 
			t->tail_ptr ++; 
		}
	}
  

}

/////////////////////////////////////////////////////////////
// Once an instruction finishes execution, mark rob entry as done
/////////////////////////////////////////////////////////////

void ROB_mark_ready(ROB *t, Inst_Info inst){
	for(int index = 0; index < NUM_ROB_ENTRIES; index ++)
	{
		if(t->ROB_Entries[index].valid && t->ROB_Entries[index].inst.inst_num == inst.inst_num)
		{
			t->ROB_Entries[index].ready = true; //marked ready
		}
	}

}

/////////////////////////////////////////////////////////////
// Find whether the prf-rob entry is ready
/////////////////////////////////////////////////////////////

bool ROB_check_ready(ROB *t, int tag){
	for(int index = 0; index < NUM_ROB_ENTRIES; index ++)
	{
		if(t->ROB_Entries[index].inst.dr_tag == tag)
		{
			return true; 
		}
	}
	return false; 

}


/////////////////////////////////////////////////////////////
// Check if the oldest ROB entry is ready for commit
/////////////////////////////////////////////////////////////

bool ROB_check_head(ROB *t){
	return (t->ROB_Entries[t->head_ptr].ready);

}

/////////////////////////////////////////////////////////////
// Remove oldest entry from ROB (after ROB_check_head)
/////////////////////////////////////////////////////////////

Inst_Info ROB_remove_head(ROB *t){
	t->ROB_Entries[t->head_ptr].valid = false; 
	if(t->head_ptr == NUM_ROB_ENTRIES -1)
	{
		t->head_ptr = 0; 
	}
	else
	{
		t->head_ptr ++; 
	}
  
}

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
