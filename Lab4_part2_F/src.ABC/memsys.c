#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "memsys.h"


//---- Cache Latencies  ------

#define DCACHE_HIT_LATENCY   1
#define ICACHE_HIT_LATENCY   1
#define L2CACHE_HIT_LATENCY  10

extern MODE   SIM_MODE;
extern uns64  CACHE_LINESIZE;
extern uns64  REPL_POLICY;

extern uns64  DCACHE_SIZE; 
extern uns64  DCACHE_ASSOC; 
extern uns64  ICACHE_SIZE; 
extern uns64  ICACHE_ASSOC; 
extern uns64  L2CACHE_SIZE; 
extern uns64  L2CACHE_ASSOC; 

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////


Memsys *memsys_new(void) 
{
  Memsys *sys = (Memsys *) calloc (1, sizeof (Memsys));

  sys->dcache = cache_new(DCACHE_SIZE, DCACHE_ASSOC, CACHE_LINESIZE, REPL_POLICY);

  if(SIM_MODE!=SIM_MODE_A){
    sys->icache = cache_new(ICACHE_SIZE, ICACHE_ASSOC, CACHE_LINESIZE, REPL_POLICY);
    sys->l2cache = cache_new(L2CACHE_SIZE, L2CACHE_ASSOC, CACHE_LINESIZE, REPL_POLICY);
    sys->dram    = dram_new();
  }

  return sys;

}


////////////////////////////////////////////////////////////////////
// This function takes an ifetch/ldst access and returns the delay
////////////////////////////////////////////////////////////////////

uns64 memsys_access(Memsys *sys, Addr addr, Access_Type type, uns core_id)
{
  uns delay=0;


  // all cache transactions happen at line granularity, so get lineaddr
  Addr lineaddr=addr/CACHE_LINESIZE;
  

  if(SIM_MODE==SIM_MODE_A){
    delay = memsys_access_modeA(sys,lineaddr,type,core_id);
  }else{
    delay = memsys_access_modeBC(sys,lineaddr,type,core_id);
  }


  //update the stats
  if(type==ACCESS_TYPE_IFETCH){
    sys->stat_ifetch_access++;
    sys->stat_ifetch_delay+=delay;
  }

  if(type==ACCESS_TYPE_LOAD){
    sys->stat_load_access++;
    sys->stat_load_delay+=delay;
  }

  if(type==ACCESS_TYPE_STORE){
    sys->stat_store_access++;
    sys->stat_store_delay+=delay;
  }


  return delay;
}



////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

void memsys_print_stats(Memsys *sys)
{
  char header[256];
  sprintf(header, "MEMSYS");

  double ifetch_delay_avg=0;
  double load_delay_avg=0;
  double store_delay_avg=0;

  if(sys->stat_ifetch_access){
    ifetch_delay_avg = (double)(sys->stat_ifetch_delay)/(double)(sys->stat_ifetch_access);
  }

  if(sys->stat_load_access){
    load_delay_avg = (double)(sys->stat_load_delay)/(double)(sys->stat_load_access);
  }

  if(sys->stat_store_access){
    store_delay_avg = (double)(sys->stat_store_delay)/(double)(sys->stat_store_access);
  }


  printf("\n");
  printf("\n%s_IFETCH_ACCESS  \t\t : %10llu",  header, sys->stat_ifetch_access);
  printf("\n%s_LOAD_ACCESS    \t\t : %10llu",  header, sys->stat_load_access);
  printf("\n%s_STORE_ACCESS   \t\t : %10llu",  header, sys->stat_store_access);
  printf("\n%s_IFETCH_AVGDELAY\t\t : %10.3f",  header, ifetch_delay_avg);
  printf("\n%s_LOAD_AVGDELAY  \t\t : %10.3f",  header, load_delay_avg);
  printf("\n%s_STORE_AVGDELAY \t\t : %10.3f",  header, store_delay_avg);
  printf("\n");

  cache_print_stats(sys->dcache, "DCACHE");

  if(SIM_MODE!=SIM_MODE_A){
    cache_print_stats(sys->icache, "ICACHE");
    cache_print_stats(sys->l2cache, "L2CACHE");
    dram_print_stats(sys->dram);
  }

}


////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

uns64 memsys_access_modeA(Memsys *sys, Addr lineaddr, Access_Type type, uns core_id){
  Flag needs_dcache_access=FALSE;
  Flag is_write=FALSE;
  
  if(type == ACCESS_TYPE_IFETCH){
    // no icache in this mode
  }
    
  if(type == ACCESS_TYPE_LOAD){
    needs_dcache_access=TRUE;
    is_write=FALSE;
  }
  
  if(type == ACCESS_TYPE_STORE){
    needs_dcache_access=TRUE;
    is_write=TRUE;
  }

  if(needs_dcache_access){
    Flag outcome=cache_access(sys->dcache, lineaddr, is_write,core_id);
    if(outcome==MISS){
      cache_install(sys->dcache, lineaddr, is_write,core_id);
    }
  }

  // timing is not simulated in Part A
  return 0;
}

////////////////////////////////////////////////////////////////////
// --------------- DO NOT CHANGE THE CODE ABOVE THIS LINE ----------
////////////////////////////////////////////////////////////////////

uns64 memsys_access_modeBC(Memsys *sys, Addr lineaddr, Access_Type type,uns core_id){
  uns64 delay=0;

  Flag needs_dcache_access=FALSE;
  Flag is_write=FALSE;
  if(type == ACCESS_TYPE_IFETCH){

    // YOU NEED TO WRITE THIS PART AND UPDATE DELAY
    delay =  delay + ICACHE_HIT_LATENCY; 
   is_write = FALSE; //we are just reading from the instruction cache, there is no need to turn is_write TRUE
  }
    

  if(type == ACCESS_TYPE_LOAD){
  	// YOU NEED TO WRITE THIS PART AND UPDATE DELAY 
  	delay = delay + DCACHE_HIT_LATENCY; 
  	is_write = FALSE;  // we will just be reading from the location and hence is_write is false. 
    needs_dcache_access = TRUE; 
  }
  

  if(type == ACCESS_TYPE_STORE){
    // YOU NEED TO WRITE THIS PART AND UPDATE DELAY
  	delay = delay + DCACHE_HIT_LATENCY; 
  	is_write = TRUE; // we will be writing to the location and hence is_write is true 
  	needs_dcache_access = TRUE; 
  }
  if(needs_dcache_access) //if we are accessing dcache 
  {
  		Flag outcome=cache_access(sys->dcache, lineaddr, is_write, core_id);
    	if(outcome == MISS){
    		//if there is a miss in L1 cache, we need to install the line into L1 cache, we have already accounteed for this delay 
      	cache_install(sys->dcache, lineaddr, is_write, core_id);
      	//if there was a miss in L1 cache, we need to see the L2 cache, if it has the line or not and if we need to install it 
      	delay = delay + memsys_L2_access(sys, lineaddr, FALSE, core_id); // just calculating the delay for a normal read from L2 cache 
      	//now see if a dirty line was evicted in Dcache 
      	if(sys->dcache->last_evicted_line.dirty == TRUE && sys->dcache->last_evicted_line.valid == TRUE)// if a dirty line was evicted , need to install in to L2 cache 
      	{
      		//write backs are done in parallel and are hence off the critical path and so we donot have to ocnsider it . 
      		uns write_back_delay = memsys_L2_access(sys, sys->dcache->last_evicted_line.tag, TRUE, core_id); 
      		//need to reset the dirty evicted lines 
      		sys->dcache->last_evicted_line.valid = FALSE; 
  			sys->dcache->last_evicted_line.dirty = FALSE; 
     	 	}
    	}
  }
  else //if we are accessing the Icache 
  {
  		Flag outcome=cache_access(sys->icache, lineaddr, is_write, core_id);
  		if(outcome == MISS){
  	//if there is a miss in L1 cache, we need to install the line into L1 cache, we have already accounteed for this delay 
  		cache_install(sys->icache, lineaddr, is_write, core_id);
      	//if there was a miss in L1 cache, we need to see the L2 cache, if it has the line or not and if we need to install it 
      	delay = delay + memsys_L2_access(sys, lineaddr, FALSE, core_id); // just calculating the delay for a normal read from L2 cache
      	if(sys->icache->last_evicted_line.dirty == TRUE && sys->icache->last_evicted_line.valid == TRUE)// if a dirty line was evicted , need to install in to L2 cache 
      	{
      		//write backs are done in parallel and are hence off the critical path and so we donot have to ocnsider it . 
      		uns write_back_delay = memsys_L2_access(sys, sys->icache->last_evicted_line.tag, TRUE, core_id); 
      		//need to reset the dirty evicted lines 
      		sys->icache->last_evicted_line.valid = FALSE; 
  			sys->icache->last_evicted_line.dirty = FALSE; 
     	 	}
    	} 

  

}
 
  return delay;
}


/////////////////////////////////////////////////////////////////////
// This function is called on ICACHE miss, DCACHE miss, DCACHE writeback
// ----- YOU NEED TO WRITE THIS FUNCTION AND UPDATE DELAY ----------
/////////////////////////////////////////////////////////////////////

uns64   memsys_L2_access(Memsys *sys, Addr lineaddr, Flag is_writeback, uns core_id){
  uns64 delay = L2CACHE_HIT_LATENCY;

  //To get the delay of L2 MISS, you must use the dram_access() function
  //To perform writebacks to memory, you must use the dram_access() function
  //This will help us track your memory reads and memory writes
  //first try and access the L2 cache and see if there is any outcome over there 
  



  Flag outcome = cache_access(sys->l2cache, lineaddr, is_writeback, core_id);  
  //if its a hit then we have already accounted for the latency above
  if(outcome == MISS) // that means the line doesnt exist in l2 cache and we need to install it into L2 dcache 
  {
  	//now need to install the line into l2 cache as well 
  	//we also simulataneously need to install the line into the L2 cache 
  	//its just a read install into the 
  	cache_install(sys->l2cache, lineaddr, is_writeback, core_id); // 
  	//need to add the delay for a dram access 
  	delay = delay + dram_access(sys->dram, lineaddr, FALSE ); //we are just trying to read the line from Dcache into L2 cache , no write back 
  	//now check the eviction that might have occured from the L2 now 
  	if(sys->l2cache->last_evicted_line.dirty == TRUE && sys->l2cache->last_evicted_line.valid == TRUE)
  	{
  		//need to do a dram access/ write to dram 
  		uns write_back_delay = dram_access(sys->dram, sys->l2cache->last_evicted_line.tag, TRUE); 
  		//lets reset the dirty evicted line of L2 cache 
  		sys->l2cache->last_evicted_line.valid = FALSE; 
  		sys->l2cache->last_evicted_line.dirty = FALSE; 
  	}

  }



  
/*
  Flag outcome = cache_access(sys->l2cache, lineaddr, is_writeback, core_id);  
  if(outcome == MISS)
  {
  	cache_install(sys->l2cache, lineaddr, is_writeback, core_id); 
  	if(sys->l2cache->last_evicted_line.dirty == TRUE && sys->l2cache->last_evicted_line.valid == TRUE)
  	{
  		//need to do a dram access/ write to dram 
  		uns write_back_delay = dram_access(sys->dram, lineaddr, TRUE); 
  		//lets reset the dirty evicted line of L2 cache 
  		sys->l2cache->last_evicted_line.valid = FALSE; 
  		sys->l2cache->last_evicted_line.dirty = FALSE; 
  	}

  }
  else
  {
  	cache_install(sys->l2cache, lineaddr, TRUE, core_id); 
  	if(sys->l2cache->last_evicted_line.dirty == TRUE && sys->l2cache->last_evicted_line.valid == TRUE)
  	{
  		//need to do a dram access/ write to dram 
  		uns write_back_delay = dram_access(sys->dram, lineaddr, TRUE); 
  		//lets reset the dirty evicted line of L2 cache 
  		sys->l2cache->last_evicted_line.valid = FALSE; 
  		sys->l2cache->last_evicted_line.dirty = FALSE; 
  	}

  }
*/





  return delay;
}
