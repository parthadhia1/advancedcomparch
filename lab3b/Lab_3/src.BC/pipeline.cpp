/***********************************************************************
 * File         : pipeline.cpp
 * Author       : Moinuddin K. Qureshi
 * Date         : 19th February 2014
 * Description  : Out of Order Pipeline for Lab3 ECE 6100
 **********************************************************************/

#include "pipeline.h"
#include <cstdlib>
#include <cstring>


extern int32_t PIPE_WIDTH;
extern int32_t SCHED_POLICY;
extern int32_t LOAD_EXE_CYCLES;
extern int32_t NUM_REST_ENTRIES;
/**********************************************************************
 * Support Function: Read 1 Trace Record From File and populate Fetch Inst
 **********************************************************************/

void pipe_fetch_inst(Pipeline *p, Pipe_Latch* fe_latch){
    static int halt_fetch = 0;
    uint8_t bytes_read = 0;
    Trace_Rec trace;
    if(halt_fetch != 1) {
      bytes_read = fread(&trace, 1, sizeof(Trace_Rec), p->tr_file);
      Inst_Info *fetch_inst = &(fe_latch->inst);
    // check for end of trace
    // Send out a dummy terminate op
      if( bytes_read < sizeof(Trace_Rec)) {
        p->halt_inst_num=p->inst_num_tracker;
        halt_fetch = 1;
        fe_latch->valid=true;
        fe_latch->inst.dest_reg = -1;
        fe_latch->inst.src1_reg = -1;
        fe_latch->inst.src1_reg = -1;
        fe_latch->inst.inst_num=-1;
        fe_latch->inst.op_type=4;
        return;
      }

    // got an instruction ... hooray!
      fe_latch->valid=true;
      fe_latch->stall=false;
      p->inst_num_tracker++;
      fetch_inst->inst_num=p->inst_num_tracker;
      fetch_inst->op_type=trace.op_type;

      fetch_inst->dest_reg=trace.dest_needed? trace.dest:-1;
      fetch_inst->src1_reg=trace.src1_needed? trace.src1_reg:-1;
      fetch_inst->src2_reg=trace.src2_needed? trace.src2_reg:-1;

      fetch_inst->dr_tag=-1;
      fetch_inst->src1_tag=-1;
      fetch_inst->src2_tag=-1;
      fetch_inst->src1_ready=false;
      fetch_inst->src2_ready=false;
      fetch_inst->exe_wait_cycles=0;
    } else {
      fe_latch->valid = false;
    }
    return; 
}


/**********************************************************************
 * Pipeline Class Member Functions 
 **********************************************************************/

Pipeline * pipe_init(FILE *tr_file_in){
    printf("\n** PIPELINE IS %d WIDE **\n\n", PIPE_WIDTH);

    // Initialize Pipeline Internals
    Pipeline *p = (Pipeline *) calloc (1, sizeof (Pipeline));
    
    p->pipe_RAT=RAT_init();
    p->pipe_ROB=ROB_init();
    p->pipe_REST=REST_init();
    p->pipe_EXEQ=EXEQ_init();
    p->tr_file = tr_file_in;
    p->halt_inst_num = ((uint64_t)-1) - 3;           
    int ii =0;
    for(ii = 0; ii < PIPE_WIDTH; ii++) {  // Loop over No of Pipes
      p->FE_latch[ii].valid = false;
      p->ID_latch[ii].valid = false;
      p->EX_latch[ii].valid = false;
      p->SC_latch[ii].valid = false;
    } 
    return p;
}


/**********************************************************************
 * Print the pipeline state (useful for debugging)
 **********************************************************************/

void pipe_print_state(Pipeline *p){
    std::cout << "--------------------------------------------" << std::endl;
    std::cout <<"cycle count : " << p->stat_num_cycle << " retired_instruction : " << p->stat_retired_inst << std::endl;
    uint8_t latch_type_i = 0;
    uint8_t width_i      = 0;
   for(latch_type_i = 0; latch_type_i < 4; latch_type_i++) {
        switch(latch_type_i) {
        case 0:
            printf(" FE: ");
            break;
        case 1:
            printf(" ID: ");
            break;
        case 2:
            printf(" SCH: ");
            break;
        case 3:
            printf(" EX: ");
            break;
        default:
            printf(" -- ");
          }
    }
   printf("\n");
   for(width_i = 0; width_i < PIPE_WIDTH; width_i++) {
       if(p->FE_latch[width_i].valid == true) {
         printf("  %d  ", (int)p->FE_latch[width_i].inst.inst_num);
       } else {
         printf(" --  ");
       }
       if(p->ID_latch[width_i].valid == true) {
         printf("  %d  ", (int)p->ID_latch[width_i].inst.inst_num);
       } else {
         printf(" --  ");
       }
       if(p->SC_latch[width_i].valid == true) {
         printf("  %d  ", (int)p->SC_latch[width_i].inst.inst_num);
       } else {
         printf(" --  ");
       }
       if(p->EX_latch[width_i].valid == true) {
         for(int ii = 0; ii < MAX_BROADCASTS; ii++) {
            if(p->EX_latch[ii].valid)
	      printf("  %d  ", (int)p->EX_latch[ii].inst.inst_num);
         }  
       } else {
         printf(" --  ");
       }
        printf("\n");
     }
     printf("\n");
      
    RAT_print_state(p->pipe_RAT);
    REST_print_state(p->pipe_REST);
   	EXEQ_print_state(p->pipe_EXEQ);
    ROB_print_state(p->pipe_ROB);
}


/**********************************************************************
 * Pipeline Main Function: Every cycle, cycle the stage 
 **********************************************************************/

void pipe_cycle(Pipeline *p)
{
    p->stat_num_cycle++;
    pipe_cycle_commit(p);
    pipe_cycle_broadcast(p);
    pipe_cycle_exe(p);
    pipe_cycle_schedule(p);
    pipe_cycle_rename(p);
    pipe_cycle_decode(p);
    pipe_cycle_fetch(p);
}

//--------------------------------------------------------------------//

void pipe_cycle_fetch(Pipeline *p){
  int ii = 0;
  Pipe_Latch fetch_latch;

  for(ii=0; ii<PIPE_WIDTH; ii++) {
    if((p->FE_latch[ii].stall) || (p->FE_latch[ii].valid)) {   // Stall 
        continue;

    } else {  // No Stall and Latch Empty
        pipe_fetch_inst(p, &fetch_latch);
        // copy the op in FE LATCH
        p->FE_latch[ii]=fetch_latch;
    }
  }
}

//--------------------------------------------------------------------//

void pipe_cycle_decode(Pipeline *p){
   int ii = 0;

   int jj = 0;

   static uint64_t start_inst_id = 1;

   // Loop Over ID Latch
   for(ii=0; ii<PIPE_WIDTH; ii++){ 
     if((p->ID_latch[ii].stall == 1) || (p->ID_latch[ii].valid)) { // Stall
       continue;  
     } else {  // No Stall & there is Space in Latch
       for(jj = 0; jj < PIPE_WIDTH; jj++) { // Loop Over FE Latch
	 if(p->FE_latch[jj].valid) {
	   if(p->FE_latch[jj].inst.inst_num == start_inst_id) { // In Order Inst Found
	     p->ID_latch[ii]        = p->FE_latch[jj];
	     p->ID_latch[ii].valid  = true;
	     p->FE_latch[jj].valid  = false;
	     start_inst_id++;
	     break;
	   }
	 }
       }
     }
   }
   
}

//--------------------------------------------------------------------//

void pipe_cycle_exe(Pipeline *p){

  int ii;
  //If all operations are single cycle, simply copy SC latches to EX latches
  if(LOAD_EXE_CYCLES == 1) {
    for(ii=0; ii<PIPE_WIDTH; ii++){
      if(p->SC_latch[ii].valid) {
        p->EX_latch[ii]=p->SC_latch[ii];
        p->EX_latch[ii].valid = true;
        p->SC_latch[ii].valid = false; 
      }
    }
    return;
  }
  
  //---------Handling exe for multicycle operations is complex, and uses EXEQ
  
  // All valid entries from SC get into exeq  
  
  for(ii = 0; ii < PIPE_WIDTH; ii++) {
    if(p->SC_latch[ii].valid) {
      EXEQ_insert(p->pipe_EXEQ, p->SC_latch[ii].inst);
      p->SC_latch[ii].valid = false;
    }
  }
  
  // Cycle the exeq, to reduce wait time for each inst by 1 cycle
  EXEQ_cycle(p->pipe_EXEQ);
  
  // Transfer all finished entries from EXEQ to EX_latch
  int index = 0;
  
  while(1) {
    if(EXEQ_check_done(p->pipe_EXEQ)) {
      p->EX_latch[index].valid = true;
      p->EX_latch[index].stall = false;
      p->EX_latch[index].inst  = EXEQ_remove(p->pipe_EXEQ);
      index++;
    } else { // No More Entry in EXEQ
      break;
    }
  }
}



/**********************************************************************
 * -----------  DO NOT MODIFY THE CODE ABOVE THIS LINE ----------------
 **********************************************************************/

void pipe_cycle_rename(Pipeline *p){

  // TODO: If src1/src2 is remapped set src1tag, src2tag
  // TODO: Find space in ROB and set drtag as such if successful
  // TODO: Find space in REST and transfer this inst (valid=1, sched=0)
  // TODO: If src1/src2 is not remapped marked as src ready
  // TODO: If src1/src2 remapped and the ROB (tag) is ready then mark src ready
  // FIXME: If there is stall, we should not do rename and ROB alloc twice
	//we will be going through the pipe width and renaming at each point 
//we just have to keep looking for the oldest instruction //we basically have to go through the ID Latch 
	int count = 0; uint64_t last_smallest = 0; int least_index = -1;
	bool stall_needed = false ; 
	while(count != PIPE_WIDTH) //smallest id number possible is -1. 
	{
		//first find the first 
		uint64_t currentmin = 0xFFFFFFFFFFFFFFFF;
		bool found_entry = false; 
		for(int i = 0; i < PIPE_WIDTH; i ++)
		{
			
			if(p->ID_latch[i].valid && p->ID_latch[i].inst.inst_num > last_smallest && p->ID_latch[i].inst.inst_num < currentmin && p->ID_latch[i].valid &&p->ID_latch[i].inst.inst_num!= 0)
			{
				currentmin = p->ID_latch[i].inst.inst_num; 
				least_index = i ; 
				found_entry = true; 
			}
		}
		if(!stall_needed && found_entry && least_index!= -1) //if stall not yet detected 
		{
			//first its best to just check if there is space in rob and rest, and if there is then only proceed
			bool rob_has_space =  ROB_check_space(p->pipe_ROB);
			bool rest_has_space = REST_check_space(p->pipe_REST); 
			if(rob_has_space && rest_has_space)
			{
			//lets first check if this instruction is currently stalled, if it is then we need to unstall this instruction 
				if(p->ID_latch[least_index].stall)
				{
				stall_needed = false; 
				p->ID_latch[least_index].stall = false; 
				p->FE_latch[least_index].stall = false; //no longer need to stall this fetch, this can go ahead too 
				}
				//now lets work on the current min, as it is the current oldest unprocessed index inside the ID latch 
				//WE ALSO HAVE TO CONSIDER THE CORNER CASES IF THE DESTINATION OR SOURCE ARE NOT NEEDED WHAT SHOULD WE DO IN THAT CASE? CREATE A FAKE ENTRY IN rob AND RES 
				//first lets do source assignment 
				int source1 = p->ID_latch[least_index].inst.src1_reg;
				int source2 = p->ID_latch[least_index].inst.src2_reg;
				if(source1 == -1) //if source 1 is not needed 
				{
					//if(p->ID_latch[least_index].inst.inst_num == 3469)
					//{
					//	printf("came inside negative source"); 
					//}
					p->ID_latch[least_index].inst.src1_ready = true; 
					p->ID_latch[least_index].inst.src1_tag = -1; 

				}
				else
				{
					p->ID_latch[least_index].inst.src1_tag = RAT_get_remap(p->pipe_RAT, source1);
					if(p->ID_latch[least_index].inst.src1_tag == -1)
					{
						p->ID_latch[least_index].inst.src1_ready = true;
					}  
					/*else
					{
						p->ID_latch[least_index].inst.src1_ready = false; //just set it initially as false 
					*/
					//check the rob if the value if already there , if yes, you can just set the source 1 as ready and tag as -ve 
					//if(p->ID_latch[least_index].inst.src2_tag != -1)
					else
					{
						if(ROB_check_ready(p->pipe_ROB, p->ID_latch[least_index].inst.src1_tag)) //if it is already ready,we can just take the value, set the ready bit and be done once and for all 
						{
							p->ID_latch[least_index].inst.src1_ready = true; 
						//	p->ID_latch[least_index].inst.src1_tag = -1; //no need to check anything again, similar to architectural register 
						}
						else
						{
							p->ID_latch[least_index].inst.src1_ready = false;
						}
					}
				}
				/*if(source2 == -1) //now same for source 2
				{
					p->ID_latch[least_index].inst.src2_ready = true; 
					p->ID_latch[least_index].inst.src2_tag = -1; 

				}	
				else
				{
					p->ID_latch[least_index].inst.src2_tag = RAT_get_remap(p->pipe_RAT, source2); 
					if(p->ID_latch[least_index].inst.src2_tag == -1)
					{
						p->ID_latch[least_index].inst.src2_ready = true;
					}  
					else
					{
						p->ID_latch[least_index].inst.src2_ready = false; //just set it initially as false 
					}
					//check the rob if the value if already there , if yes, you can just set the source 1 as ready and tag as -ve 
					if(p->ID_latch[least_index].inst.src2_tag != -1)
					{
					if(ROB_check_ready(p->pipe_ROB, p->ID_latch[least_index].inst.src2_tag)) //if it is already ready,we can just take the value, set the ready bit and be done once and for all 
					{
						p->ID_latch[least_index].inst.src2_ready = true; 
						p->ID_latch[least_index].inst.src2_tag = -1; //no need to check anything again, similar to architectural register 
					}
				}
				}*/
				if(source2 == -1) //if source 1 is not needed 
				{
					//if(p->ID_latch[least_index].inst.inst_num == 3469)
					//{
					//	printf("came inside negative source"); 
					//}
					p->ID_latch[least_index].inst.src2_ready = true; 
					p->ID_latch[least_index].inst.src2_tag = -1; 

				}
				else
				{
					p->ID_latch[least_index].inst.src2_tag = RAT_get_remap(p->pipe_RAT, source1);
					if(p->ID_latch[least_index].inst.src2_tag == -1)
					{
						p->ID_latch[least_index].inst.src2_ready = true;
					}  
					/*else
					{
						p->ID_latch[least_index].inst.src1_ready = false; //just set it initially as false 
					*/
					//check the rob if the value if already there , if yes, you can just set the source 1 as ready and tag as -ve 
					//if(p->ID_latch[least_index].inst.src2_tag != -1)
					else
					{
						if(ROB_check_ready(p->pipe_ROB, p->ID_latch[least_index].inst.src2_tag)) //if it is already ready,we can just take the value, set the ready bit and be done once and for all 
						{
							p->ID_latch[least_index].inst.src2_ready = true; 
							//p->ID_latch[least_index].inst.src2_tag = -1; //no need to check anything again, similar to architectural register 
						}
						else
						{
							p->ID_latch[least_index].inst.src2_ready = false;
						}
				
					}
				}
				// now that source is done, we can can move on to the destination stuff 
				int dest = p->ID_latch[least_index].inst.dest_reg; 
				if(dest != -1)
				{
					//assign the rob entry first 
					int index_inserted = ROB_insert(p->pipe_ROB, p->ID_latch[least_index].inst);
					//rename inside the rat 
					p->ID_latch[least_index].inst.dr_tag = index_inserted;  //the ID latch is now ready 
					RAT_set_remap(p->pipe_RAT, p->ID_latch[least_index].inst.dest_reg, index_inserted ); 
				}
				else
				{
					//just create a fake entry in rob if destination is not needed 
					p->ID_latch[least_index].inst.dr_tag = -1; 
					int m = ROB_insert(p->pipe_ROB, p->ID_latch[least_index].inst); //rememeber .................. instuction with no branch will have -1 for dr tag
					if(m == -1)
					{
						printf("ROB NOT WORKING"); 
					}
				}
				//now that the rob has been inserted and stuff, its time to take on REST 
				REST_insert(p->pipe_REST, p->ID_latch[least_index].inst); 
				p->ID_latch[least_index].valid = false; 
				//we can now move on to the next instruction
			}
			else // if we have run out of space in any one structure, we need to declare stall everywhere and back propogate the stalls 
			{
				stall_needed = true; 
				p->ID_latch[least_index].stall = true; 
				p->FE_latch[least_index].stall = true; 
			}
		}
		else
		{
			p->ID_latch[least_index].stall = true; 
			p->FE_latch[least_index].stall = true; 
		}
		//now change the last min and stuff to move on to the next instruction 
		last_smallest = currentmin; 
		count ++; 
	}
	//we donot need to stall all the fetches, for all the instructions that have been scheduled, we can let new instructions come in freely, always the oldest will be scheduled first inside the reservation station first, so let them come in brother! 
	/*if(stall_needed)
	{
		for(int index = 0; index < PIPE_WIDTH; index ++)
		{
			p->FE_latch[index].stall = true; 
		}
	}*/


}

//--------------------------------------------------------------------//
void pipe_cycle_schedule(Pipeline *p){

  // TODO: Implement two scheduling policies (SCHED_POLICY: 0 and 1)
	//for schedule I donot think we need to keep finding the oldest instruction ,we could just say that if already scheduled donot go for it i guess. but we have to find the smallest instruction first, try that method out if the current method doesnt work. 

  if(SCHED_POLICY==0){
    // inorder scheduling
    // Find all valid entries, if oldest is stalled then stop
    // Else send it out and mark it as scheduled
    int count = 0; 
    while(count != PIPE_WIDTH)
    {
    	int least_index = -1; uint64_t last_num = 0xFFFFFFFFFFFFFFFF; 
    	//find the oldest valid instruction in the reservation station
    	bool found_entry = false; 
    	for(int ii = 0; ii < NUM_REST_ENTRIES; ii ++)
    	{
    		if(!p->pipe_REST->REST_Entries[ii].scheduled && p->pipe_REST->REST_Entries[ii].valid && last_num > p->pipe_REST->REST_Entries[ii].inst.inst_num )
    		{
    			least_index = ii; 
    			last_num = p->pipe_REST->REST_Entries[ii].inst.inst_num; 
    			found_entry = true; 
    		}
    	}
    	if(least_index != -1 && found_entry)
    	{
    		if(p->pipe_REST->REST_Entries[least_index].inst.src1_ready &&  p->pipe_REST->REST_Entries[least_index].inst.src2_ready)
    		{
    			p->SC_latch[count].inst = p->pipe_REST->REST_Entries[least_index].inst; 
    			p->SC_latch[count].valid = true; 
    			REST_schedule(p->pipe_REST, p->pipe_REST->REST_Entries[least_index].inst);
    			//p->pipe_REST->REST_Entries[least_index].scheduled = true;  //lets use the API above to schedule the thing brother, why mess it up manually 
    			count ++; 
    			//we have to reset the least index and the last_num for the next search for unscheduled valid oldest instruction in REST
    			least_index = -1; last_num = 0xFFFFFFFFFFFFFFFF;  //this shouldnt have been needed, as we are defining the variables inside the loop, but just incase u know 
    		}
    		else
    		{
    			count = PIPE_WIDTH; 
    			break ; 
    			//time to get out. 
    		}
    	}
    	else
    	{
    		count = PIPE_WIDTH; //if an entry wasnt found that was possible if least index is still -1 or if there is simply no valid unscheduled entry , then we need to get the hell out of there
    		break ; 
    	}
    }
  }

  if(SCHED_POLICY==1){
    // out of order scheduling
    // Find valid/unscheduled/src1ready/src2ready entries in REST
    // Transfer them to SC_latch and mark that REST entry as scheduled
    int count = 0; uint64_t last_smallest = 0; 
    while(count != PIPE_WIDTH)
    {
    	int least_index = -1; uint64_t last_num = 0xFFFFFFFFFFFFFFFF; 
    	//find the oldest instruction in the reservation station
    	bool found_entry = false; 
    	for(int ii = 0; ii < NUM_REST_ENTRIES; ii ++)
    	{
    		if(!p->pipe_REST->REST_Entries[ii].scheduled && p->pipe_REST->REST_Entries[ii].valid && last_num > p->pipe_REST->REST_Entries[ii].inst.inst_num && p->pipe_REST->REST_Entries[ii].inst.inst_num > last_smallest)
    		{
    			least_index = ii; 
    			last_num = p->pipe_REST->REST_Entries[ii].inst.inst_num;
    			found_entry = true; 
    		}
    	}
    	//got the oldest unscheduled instruction we haven't examined yet
    	if(found_entry && least_index != -1)
    	{
    		if(p->pipe_REST->REST_Entries[least_index].inst.src1_ready &&  p->pipe_REST->REST_Entries[least_index].inst.src2_ready) //if both the sources are ready time to schedule it
    		{
    			p->SC_latch[count].inst = p->pipe_REST->REST_Entries[least_index].inst; 
    			p->SC_latch[count].valid = true; 
    			REST_schedule(p->pipe_REST, p->pipe_REST->REST_Entries[least_index].inst);
    			//p->pipe_REST->REST_Entries[least_index].scheduled = true;  //lets use the API above to schedule the thing brother, why mess it up manually 
    			count ++; 
    		}
    		//scheduled or not we need to move on to the next instruction //we have to reset the least index and the last_num for the next search for unscheduled valid oldest instruction in REST
    		last_smallest = last_num; least_index = -1; last_num = 0xFFFFFFFFFFFFFFFF;  //this shouldnt have been needed, as we are defining the variables inside the loop, but just incase u know 
    	}
    	else //no valid instruction found, time to move out and move on 
    	{
    		count = PIPE_WIDTH; 
    		break; 
    	}
    }

  }
}


//--------------------------------------------------------------------//

void pipe_cycle_broadcast(Pipeline *p){

  // TODO: Go through all instructions out of EXE latch
  // TODO: Broadcast it to REST (using wakeup function)
  // TODO: Remove entry from REST (using inst_num)
  // TODO: Update the ROB, mark ready, and update Inst Info in ROB
	for(int ii = 0; ii < MAX_BROADCASTS; ii ++)
	{
		//wake up the rest using the dr_tag 
		if(p->EX_latch[ii].inst.dr_tag != -1) //if the instruction has a destination tag then 
		{
			REST_wakeup(p->pipe_REST, p->EX_latch[ii].inst.dr_tag);
	    }
	    
	    //wake up done, time to remove the instructions out of REST using inst num 
		REST_remove(p->pipe_REST, p->EX_latch[ii].inst); //entry removed. 
		//now update the ROB using the inst num marking it as ready 
		ROB_mark_ready(p->pipe_ROB, p->EX_latch[ii].inst);
		//also have to mark the execute latch as false tr
		//invalidate ex latches on broadcast 
		p->EX_latch[ii].valid = false; 
		
	}
}


//--------------------------------------------------------------------//


void pipe_cycle_commit(Pipeline *p) {
  // TODO: check the head of the ROB. If ready commit (update stats)
  // TODO: Deallocate entry from ROB
  // TODO: Update RAT after checking if the mapping is still valid

  // DUMMY CODE (for compiling, and ensuring simulation terminates!)
int counter = 0;
while(ROB_check_head(p->pipe_ROB) && counter < PIPE_WIDTH)
{
	p->stat_retired_inst++; //instruction committed 
	Inst_Info remove_inst = ROB_remove_head(p->pipe_ROB); 
	if(remove_inst.dest_reg != -1)
  	{
  		if(RAT_get_remap(p->pipe_RAT, remove_inst.dest_reg) == remove_inst.dr_tag)
  		{
  			RAT_reset_entry(p->pipe_RAT, remove_inst.dest_reg); 
  		}
  	}
  	counter ++; 
  	//check if it is time to halt 
  	if(remove_inst.inst_num >= p->halt_inst_num)
  	{
  		p->halt=true;
  		counter = PIPE_WIDTH; 
  		break;
  	}
}

  /*for(ii=0; ii<PIPE_WIDTH; ii++){
    if(p->FE_latch[ii].valid){
      if(p->FE_latch[ii].inst.inst_num >= p->halt_inst_num){
        p->halt=true;
      }else{
	p->stat_retired_inst++;
	p->FE_latch[ii].valid=false;
      }
    }
  }*/
}
  
//--------------------------------------------------------------------//




