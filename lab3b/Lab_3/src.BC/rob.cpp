#include <stdio.h>
#include <assert.h>

#include "rob.h"


extern int32_t NUM_ROB_ENTRIES;

/////////////////////////////////////////////////////////////
// Init function initializes the ROB
/////////////////////////////////////////////////////////////

ROB* ROB_init(void){
  int ii;
  ROB *t = (ROB *) calloc (1, sizeof (ROB));
  for(ii=0; ii<MAX_ROB_ENTRIES; ii++){
    t->ROB_Entries[ii].valid=false;
    t->ROB_Entries[ii].ready=false;
  }
  t->head_ptr=0;
  t->tail_ptr=0;
  return t;
}

/////////////////////////////////////////////////////////////
// Print State
/////////////////////////////////////////////////////////////
void ROB_print_state(ROB *t){
 int ii = 0;
  printf("Printing ROB \n");
  printf("Entry  Inst   Valid   ready\n");
  for(ii = 0; ii < 32; ii++) {
    printf("%5d ::  %d\t", ii, (int)t->ROB_Entries[ii].inst.inst_num);
    printf(" %5d\t", t->ROB_Entries[ii].valid);
    printf(" %5d\n", t->ROB_Entries[ii].ready);
  }
  printf("\n");
}

/////////////////////////////////////////////////////////////
//------- DO NOT CHANGE THE CODE ABOVE THIS LINE -----------
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// If there is space in ROB return true, else false
/////////////////////////////////////////////////////////////

bool ROB_check_space(ROB *t){
	/*for(int index = 0; index < NUM_ROB_ENTRIES; index ++)
	{
		if(!t->ROB_Entries[index].valid) //if not valid, it means there is space 
		{
			return true; 
		}
	}*///t->tail_ptr == t->head_ptr &&
	/*if( t->ROB_Entries[t->tail_ptr].valid) //if the head is equal to the tail and the tail entry is a valid entry that means there is no space for new things 
	{
		return false; 
	}*/
	/*if(t->tail_ptr == NUM_ROB_ENTRIES)
	{
		if(t->ROB_Entries[0].valid)
		{
			return false
		}
	}
	return true; //means no empty space 
*/
	if(t->ROB_Entries[t->tail_ptr].valid)
	{
		return false; 
	}
	return true; 
}

/////////////////////////////////////////////////////////////
// insert entry at tail, increment tail (do check_space first)
/////////////////////////////////////////////////////////////

int ROB_insert(ROB *t, Inst_Info inst){
	if(ROB_check_space(t)) //if there is space 
	{
		if(t->tail_ptr == NUM_ROB_ENTRIES - 1) //if its reached at the bottom
		{
			int value = NUM_ROB_ENTRIES-1 ; 
			inst.dr_tag = value; 
			t->ROB_Entries[value].inst = inst; 
			t->ROB_Entries[value].valid = true; 
			t->ROB_Entries[value].ready = false; 
			t->tail_ptr = 0; 
			return NUM_ROB_ENTRIES-1; 
		}
		else
		{
			inst.dr_tag = t->tail_ptr; 
			t->ROB_Entries[t->tail_ptr].inst = inst;
			t->ROB_Entries[t->tail_ptr].valid = true;  
			t->ROB_Entries[t->tail_ptr].ready = false;
			int return_value = t->tail_ptr;  
			t->tail_ptr ++; 
			return return_value;  
		}
	}
	return -1; 
  

}

/////////////////////////////////////////////////////////////
// Once an instruction finishes execution, mark rob entry as done
/////////////////////////////////////////////////////////////

void ROB_mark_ready(ROB *t, Inst_Info inst){
	for(int ii = 0; ii < NUM_ROB_ENTRIES; ii ++)
	{
		if(t->ROB_Entries[ii].inst.inst_num == inst.inst_num && t->ROB_Entries[ii].valid)
		{
			t->ROB_Entries[ii].ready = true; //marked ready
			break; 
		}
	}

}

/////////////////////////////////////////////////////////////
// Find whether the prf-rob entry is ready
/////////////////////////////////////////////////////////////

bool ROB_check_ready(ROB *t, int tag){
	/*for(int index = 0; index < NUM_ROB_ENTRIES; index ++)
	{
		if(t->ROB_Entries[index].inst.dr_tag == tag && t->ROB_Entries[index].ready)
		{
			return true; 
		}
	}
	return false; 
	*/
	//above thing was wrong, just index into the tag and check the ready bit lol 




	//just check if its valid and if its ready to be committed 
	//just make sure to deallocate out of RAT if still in RAT on committing otherwise cause load of problems. 
	return (t->ROB_Entries[tag].ready); 
}


/////////////////////////////////////////////////////////////
// Check if the oldest ROB entry is ready for commit
/////////////////////////////////////////////////////////////

bool ROB_check_head(ROB *t){
	return (t->ROB_Entries[t->head_ptr].ready );//&& t->ROB_Entries[t->head_ptr].valid);

}

/////////////////////////////////////////////////////////////
// Remove oldest entry from ROB (after ROB_check_head)
/////////////////////////////////////////////////////////////

Inst_Info ROB_remove_head(ROB *t){
	Inst_Info removal = t->ROB_Entries[t->head_ptr].inst; 
	t->ROB_Entries[t->head_ptr].valid = false; 
	t->ROB_Entries[t->head_ptr].ready = false; 
	if(t->head_ptr == NUM_ROB_ENTRIES -1)
	{
		t->head_ptr = 0; 
	}
	else
	{
		t->head_ptr ++; 
	}
  return removal; 
}

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
