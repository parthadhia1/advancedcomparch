#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "cache.h"


extern uns64 SWP_CORE0_WAYS; // Input Way partitions for Core 0 
extern uns64 SWP_core0ways;//just added 
extern uns64 cycle; // You can use this as timestamp for LRU

////////////////////////////////////////////////////////////////////
// ------------- DO NOT MODIFY THE INIT FUNCTION -----------
////////////////////////////////////////////////////////////////////

Cache  *cache_new(uns64 size, uns64 assoc, uns64 linesize, uns64 repl_policy){

   Cache *c = (Cache *) calloc (1, sizeof (Cache));
   c->num_ways = assoc;
   c->repl_policy = repl_policy;

   if(c->num_ways > MAX_WAYS){
     printf("Change MAX_WAYS in cache.h to support %llu ways\n", c->num_ways);
     exit(-1);
   }

   // determine num sets, and init the cache
   c->num_sets = size/(linesize*assoc);
   c->sets  = (Cache_Set *) calloc (c->num_sets, sizeof(Cache_Set));

   return c;
}

////////////////////////////////////////////////////////////////////
// ------------- DO NOT MODIFY THE PRINT STATS FUNCTION -----------
////////////////////////////////////////////////////////////////////

void    cache_print_stats    (Cache *c, char *header){
  double read_mr =0;
  double write_mr =0;

  if(c->stat_read_access){
    read_mr=(double)(c->stat_read_miss)/(double)(c->stat_read_access);
  }

  if(c->stat_write_access){
    write_mr=(double)(c->stat_write_miss)/(double)(c->stat_write_access);
  }

  printf("\n%s_READ_ACCESS    \t\t : %10llu", header, c->stat_read_access);
  printf("\n%s_WRITE_ACCESS   \t\t : %10llu", header, c->stat_write_access);
  printf("\n%s_READ_MISS      \t\t : %10llu", header, c->stat_read_miss);
  printf("\n%s_WRITE_MISS     \t\t : %10llu", header, c->stat_write_miss);
  printf("\n%s_READ_MISSPERC  \t\t : %10.3f", header, 100*read_mr);
  printf("\n%s_WRITE_MISSPERC \t\t : %10.3f", header, 100*write_mr);
  printf("\n%s_DIRTY_EVICTS   \t\t : %10llu", header, c->stat_dirty_evicts);

  printf("\n");
}



////////////////////////////////////////////////////////////////////
// Note: the system provides the cache with the line address
// Return HIT if access hits in the cache, MISS otherwise 
// Also if is_write is TRUE, then mark the resident line as dirty
// Update appropriate stats
////////////////////////////////////////////////////////////////////

Flag cache_access(Cache *c, Addr lineaddr, uns is_write, uns core_id){
  Flag outcome=MISS;

  // Your Code Goes Here
//go through the cache first 


  // so we can access the cache using the size of the cache. // we
  // index using the number of sets. //need to find the log to the
  // base of 2 for the number of ways and then use it to access the cache 
  /*int number_of_bits = 0; int size = c->num_sets; 
  while(size > 1)
  {
      size = size / 2; 
      number_of_bits ++; 
  }*/



  uns set_index = lineaddr % c->num_sets; 
  //  printf(" line address is %llu\n",lineaddr); 
  //  printf("inside the cache line\n");
  //we can now use the number of bits of the line address to access the cache set 
  uns jj = 0; 
    for( jj = 0 ; jj < c->num_ways; jj++)
    {
      //printf("%llu ",c->sets[set_index].line[jj].tag); 
    
      if(c->sets[set_index].line[jj].tag == lineaddr && c->sets[set_index].line[jj].valid == TRUE)
      {
      //  printf("HIT\n");
        outcome = HIT; 
        break; 
      }
    }
  //if (outcome == MISS)
 // {
    if(is_write == TRUE)
    {
      //c->stat_write_miss ++; 
      c->stat_write_access ++; 
    }
    else
    {
      //c->stat_read_miss ++;
      c->stat_read_access ++; 
    }
    if(outcome == HIT)
    {
      //do the LRU thing here itself 
      for(uns m = 0; m < c->num_ways; m++)
  {
    c->sets[set_index].line[m].last_access_time ++; 
  }
  c->sets[set_index].line[jj].last_access_time = 1; 

if(is_write == TRUE)
    {
      //c->stat_write_miss ++; 
      c->sets[set_index].line[jj].dirty = TRUE;
    }
    else
    {
      //c->stat_read_miss ++;
      //if(c->sets[set_index].line[jj].dirty == FALSE;
      //c->sets[set_index].line[jj].dirty = FALSE;
    }





    }
  /*}
  else
  {
    if(is_write == TRUE)
    {
      c->stat_write_access ++; 
    }
    else
    {
      c->stat_read_access ++; 
    }

  }*/

  return outcome;
}

////////////////////////////////////////////////////////////////////
// Note: the system provides the cache with the line address
// Install the line: determine victim using repl policy (LRU/RAND)
// copy victim into last_evicted_line for tracking writebacks
////////////////////////////////////////////////////////////////////

void cache_install(Cache *c, Addr lineaddr, uns is_write, uns core_id){

  // Your Code Goes Here
  // Find victim using cache_find_victim
  // Initialize the evicted entry
  // Initialize the victime entry
  //for whatever core, if there a place that is free, then just insert it and also keep a track of number of data points 
    uns set_index = lineaddr % c->num_sets;  
  int insert_index = -1; 
  uns way_index = 0; 
  for (way_index = 0; way_index < c->num_ways; way_index ++)
  {
    if(c->sets[set_index].line[way_index].valid == FALSE)
    {
      //found a place to insert , lets insert 
      c->sets[set_index].line[way_index].valid = TRUE; 
      if(is_write == TRUE)
      {
        c->stat_write_miss ++; 
        c->sets[set_index].line[way_index].dirty = TRUE; 
      }
      else
      {
        c->stat_read_miss ++; //no need to set anything as not dirty if we 
        c->sets[set_index].line[way_index].dirty = FALSE;
      }
      c->sets[set_index].line[way_index].tag = lineaddr;
      c->sets[set_index].line[way_index].core_id = core_id; 
      if(c->repl_policy != 2)
      {

        for(uns m = 0; m < c->num_ways; m++)
        {
            if(c->sets[set_index].line[m].valid)
            {
                  c->sets[set_index].line[m].last_access_time ++; 
            }
        }
      }
      else
      {
        for(uns m = 0; m < c->num_ways; m++)
        {
            if(c->sets[set_index].line[m].core_id == core_id && c->sets[set_index].line[m].valid)
            {
                c->sets[set_index].line[m].last_access_time ++; 
            }
        }
      }
      insert_index = 0; 
      //way_index = c -> num_ways; 
      break ;

    }
  } //if there is no free spot found then. 
  if(insert_index == -1)
  {
    way_index = cache_find_victim(c, set_index, core_id);
    c->last_evicted_line = c->sets[set_index].line[way_index];
    if(c->last_evicted_line.dirty == TRUE)
      {
         c->stat_dirty_evicts ++; 
      }

    c->sets[set_index].line[way_index].valid = TRUE; 
      if(is_write == TRUE)
      {
        c->stat_write_miss ++; 
        c->sets[set_index].line[way_index].dirty = TRUE; 
      }
      else
      {
        c->stat_read_miss ++;
        //if(c->sets[set_index].line[way_index].dirty == FALSE)
        //{
        c->sets[set_index].line[way_index].dirty = FALSE;
      //}
      }
      c->sets[set_index].line[way_index].tag = lineaddr;
      c->sets[set_index].line[way_index].core_id = core_id;  
  }
/*for(uns m = 0; m < c->num_ways; m++)
  {
    if(c->sets[set_index].line[m].core_id == core_id && c->sets[set_index].line[way_index].valid)
    {
    c->sets[set_index].line[m].last_access_time ++; 
  }
  }*/
  c->sets[set_index].line[way_index].last_access_time = 1; 
}

////////////////////////////////////////////////////////////////////
// You may find it useful to split victim selection from install
////////////////////////////////////////////////////////////////////


uns cache_find_victim(Cache *c, uns set_index, uns core_id){
  uns victim=0;

  // TODO: Write this using a switch case statement  
  if(c->repl_policy == 0)
  {
    for(uns access_index = 0; access_index < c->num_ways; access_index ++)
  {
    if(c->sets[set_index].line[access_index].valid && c->sets[set_index].line[access_index].last_access_time > c->sets[set_index].line[victim].last_access_time)
    {
      victim = access_index;
    }
  }
  for(uns m = 0; m < c->num_ways; m++)
        {
            if(c->sets[set_index].line[m].valid)
            {
                  c->sets[set_index].line[m].last_access_time ++; 
            }
        }


  }
else if(c->repl_policy == 2)
{
 /* for(uns access_index = 0; access_index < c->num_ways; access_index ++)
  {
    if(c->sets[set_index].line[access_index].last_access_time > c->sets[set_index].line[victim].last_access_time)
    {
      victim = access_index;
    } 
  }*/
  //we have come here because there is no free spot my dear friend 
  //so first check the number of ways that one core has over the other core. 
  uns core1_ways = 0; 
  uns core2_ways = 0; 
  for(uns access_index = 0; access_index < c->num_ways; access_index ++)
  {
    if(c->sets[set_index].line[access_index].core_id == 0)
    {
      core1_ways ++; 
    }
    else
    {
      core2_ways ++; 
    }
  }
  //so now we have the total number of ways. 
  if(core1_ways > SWP_CORE0_WAYS) 
  { 
    //whatever core might be accessing the victcim has to be from core 0. 
      //first find a cache line of core 0
      for(uns i = 0; i < c->num_ways; i ++)
      {
        if(c->sets[set_index].line[i].core_id == 0)
        {
          victim = i; 
          break; 
        }
      }

      for(uns c0_index = 0; c0_index < c->num_ways; c0_index ++)
      {
          if(c->sets[set_index].line[c0_index].core_id == 0 && c->sets[set_index].line[c0_index].last_access_time > c->sets[set_index].line[victim].last_access_time)
          {
              victim = c0_index;
           } 
       }
    }
    else if(core1_ways < SWP_CORE0_WAYS) //time to remove from core 1 
    {
        //whatever core might be accessing the victcim has to be from core 0. 
      //first find a cache line of core 0
      for(uns i = 0; i < c->num_ways; i ++)
      {
        if(c->sets[set_index].line[i].core_id == 1)
        {
          victim = i; 
          break; 
        }
      }

      for(uns c0_index = 0; c0_index < c->num_ways; c0_index ++)
      {
          if(c->sets[set_index].line[c0_index].core_id == 1 && c->sets[set_index].line[c0_index].last_access_time > c->sets[set_index].line[victim].last_access_time)
          {
              victim = c0_index;
           } 
       }
    }
    else //if they are equal then we just take one out from their own respective cores. 
    {

 //whatever core might be accessing the victcim has to be from core 0. 
      //first find a cache line of core 0
      for(uns i = 0; i < c->num_ways; i ++)
      {
        if(c->sets[set_index].line[i].core_id == core_id)
        {
          victim = i; 
          break; 
        }
      }

      for(uns c1_index = 0; c1_index < c->num_ways; c1_index ++)
      {
          if(c->sets[set_index].line[c1_index].core_id == core_id && c->sets[set_index].line[c1_index].last_access_time > c->sets[set_index].line[victim].last_access_time)
          {
              victim = c1_index;
           } 
       }

  }
  for(uns m = 0; m < c->num_ways; m++)
  {
    if(c->sets[set_index].line[m].core_id == core_id && c->sets[set_index].line[m].valid)
    {
    c->sets[set_index].line[m].last_access_time ++; 
  }
  }
}
else
{
  victim = (uns64) rand() % c->num_ways;
}
  /*if(c->sets[set_index].line[victim].dirty == TRUE)
  {
          c->stat_dirty_evicts ++; 
  }*/

  return victim;
}

